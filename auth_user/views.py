from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
# Create your views here.

def login(request):
    template = loader.get_template('auth_html/login/login.html')
    return HttpResponse(template.render())

def signin(request):
    template = loader.get_template('auth_html/signin/sign_in.html')
    return HttpResponse(template.render())