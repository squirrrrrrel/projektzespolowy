from django.urls import path
from django.conf.urls import url
from . import views
from django.conf.urls.static import static
from django.conf import settings
import os
urlpatterns = [
	path('login/', views.login, name = 'login'),
	path('signin/', views.signin, name = 'signin'),
]